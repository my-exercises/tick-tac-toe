package be.exercise.myexercise

import be.exercise.myexercise.board.di.boardModule
import be.exercise.myexercise.core.app.CoreAppController
import org.koin.core.module.Module

class AppController : CoreAppController() {
    override val diModules: List<Module> by lazy {
        listOf(
            boardModule
        )
    }
}