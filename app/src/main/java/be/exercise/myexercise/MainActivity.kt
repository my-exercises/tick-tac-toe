package be.exercise.myexercise

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import be.exercise.myexercise.navigation.NavigationAction
import be.exercise.myexercise.navigation.extensions.getNavigationHost
import be.exercise.myexercise.navigation.navigateTo
import timber.log.Timber

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        try {
            val data = intent?.data ?: return Timber.i("Deep link not found")
            val navigationAction = NavigationAction.Uri(data.toString())
            val navHostFragment = getNavigationHost(R.id.container)

            if (navHostFragment != null) {
                navHostFragment.navigateTo(navigationAction)
            } else {
                Timber.w("NavHostFragment not found to handle deep link")
            }
        } catch (e: Exception) {
            Timber.w(e, "Deep link handle failed ")
        }
    }
}