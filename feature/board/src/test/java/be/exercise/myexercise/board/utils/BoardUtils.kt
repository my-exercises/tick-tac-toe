package be.exercise.myexercise.board.utils

import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointState
import be.exercise.myexercise.data.model.BoardPointsListRepresentation


/**
 * It's easier and faster to create test and understand board points position in a more human readable format
 * Convert human readable board representation to android operational representation
 * ,or
 * x,_,_
 * o,_,x
 * o,x,o to [BoardPointsListRepresentation]
 */
val String.asBoard: BoardPointsListRepresentation
    get() {
        // "x,_,_","o,_,x","o,x,o"
        val rows = this.split('\n')
        return List(rows.size) { column ->
            List(rows.size) { row ->
                val pointState = rows[column].split(',')[row].asBoardPointState
                BoardPoint(column, row, pointState)
            }
        }
    }

val BoardPointsListRepresentation.asString: String
    get() = buildString {
        this@asString.forEach { column ->
            append(column.map { it.state.asHumanReadable }.reduce { acc, s -> "$acc,$s" })
            append("\n")
        }
    }.trimIndent()

val String.asBoardPointState: BoardPointState
    get() = when (lowercase()) {
        "x" -> BoardPointState.CROSS
        "o" -> BoardPointState.NOUGHT
        else -> BoardPointState.EMPTY
    }

val BoardPointState.asHumanReadable: String
    get() = when (this) {
        BoardPointState.EMPTY -> "_"
        BoardPointState.NOUGHT -> "o"
        BoardPointState.CROSS -> "x"
    }