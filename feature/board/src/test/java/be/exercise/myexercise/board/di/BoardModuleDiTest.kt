package be.exercise.myexercise.board.di

import be.exercise.myexercise.board.BoardViewModel
import be.exercise.myexercise.board.gamerules.GameRules
import be.exercise.myexercise.board.repository.BoardRepository
import kotlinx.coroutines.CoroutineDispatcher
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.koin.core.logger.Level
import org.koin.test.*
import org.koin.test.category.CheckModuleTest
import org.koin.test.check.checkModules
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito

@Category(CheckModuleTest::class)
class BoardModuleDiTest : AutoCloseKoinTest() {
    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger(Level.ERROR)
        modules(boardModule)
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @Test
    fun `Verify core module`() {
        checkModules {
            modules(boardModule)
        }
    }

    @Test
    fun `Verify GameRules is declared`() {
        assertNotNull(get<GameRules>())
    }

    @Test
    fun `Verify BoardRepository is declared`() {
        assertNotNull(get<BoardRepository>())
    }

    /**
     * Reference for koin mocks https://insert-koin.io/docs/reference/koin-test/testing
     */
    @Test
    fun `Verify BoardViewModel is declared`() {
        // Because Coroutine Dispatcher is declared in CoreModule,
        // we need to mock it for ViewModel as we test only board module here
        declareMock<CoroutineDispatcher>()

        assertNotNull(get<BoardViewModel>())
    }
}