package be.exercise.myexercise.board

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import be.exercise.myexercise.board.gamerules.GameRulesImpl
import be.exercise.myexercise.board.repository.BoardRepositoryImpl
import be.exercise.myexercise.core.base.Effect
import be.exercise.myexercise.core.base.State
import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.Assert.*
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
class BoardViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val dispatcher = TestCoroutineDispatcher()

    companion object {
        private lateinit var stateObserver: Observer<State>
        private lateinit var effectObserver: Observer<Effect>
        private lateinit var viewModel: BoardViewModel

        @BeforeClass
        @JvmStatic
        fun initialize() {
            viewModel = BoardViewModel(
                Dispatchers.Main,
                BoardRepositoryImpl(),
                GameRulesImpl()
            )
            stateObserver = mock()
            effectObserver = mock()
        }

        @JvmStatic
        fun resetMocks() {
            reset(stateObserver)
            reset(effectObserver)

            viewModel.state.observeForever(stateObserver)
            viewModel.effect.observeForever(effectObserver)
        }

        @JvmStatic
        fun createBoardViewModel() {
            viewModel = BoardViewModel(
                Dispatchers.Main,
                BoardRepositoryImpl(),
                GameRulesImpl()
            )
        }
    }

    @Before
    fun beforeEach() {
        Dispatchers.setMain(dispatcher)
        createBoardViewModel()
        resetMocks()
    }

    @Test
    fun `Verify points update dispatched after placing point`() {
        val placedPoint = BoardPoint(0, 0, BoardPointState.CROSS)

        viewModel.processAction(BoardAction.PointClicked(placedPoint))

        val captor = ArgumentCaptor.forClass(BoardState::class.java)
        captor.run {
            verify(stateObserver, atLeastOnce()).onChanged(capture())
            val keptValue = value
            if(keptValue is BoardState.PointsUpdated) {
                assertNotNull(keptValue.points.find { it == placedPoint })
            }
        }
    }

    @Test
    fun `Verify turn change is dispatched after placing point`() {
        val givenTurn = BoardPointState.CROSS
        val expectedTurn = BoardPointState.NOUGHT

        viewModel.processAction(BoardAction.PointClicked(BoardPoint(0, 0, givenTurn)))

        val captor = ArgumentCaptor.forClass(BoardState.TurnChanged::class.java)
        captor.run {
            verify(stateObserver, atLeastOnce()).onChanged(capture())
            assertEquals(expectedTurn, value.turn)
        }
    }

    @Test
    fun `Verify board resets correctly`() {
        viewModel.processAction(BoardAction.PointClicked(BoardPoint(0, 0, BoardPointState.CROSS)))
        viewModel.processAction(BoardAction.ResetBoard)

        val captor = ArgumentCaptor.forClass(BoardState.PointsUpdated::class.java)
        captor.run {
            verify(stateObserver, atLeastOnce()).onChanged(capture())
            assertNull(value.points.find { !it.isEmpty })
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}