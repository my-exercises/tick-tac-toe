package be.exercise.myexercise.board.gamerules

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import be.exercise.myexercise.board.utils.asBoard
import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.*
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class GameRulesTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    companion object {
        private lateinit var gameRules: GameRules

        @BeforeClass
        @JvmStatic
        fun initialize() {
            gameRules = GameRulesImpl()
        }
    }

    @Before
    fun beforeEach() {
        gameRules.reset()
    }

    @Test
    fun `Turn moved to next user`() {
        val givenTurn = BoardPointState.CROSS
        val expectedNextTurn = BoardPointState.NOUGHT

        gameRules.moveToNextTurn(givenTurn)

        assertEquals(expectedNextTurn, gameRules.currentUserTurn)
    }


    @Test
    fun `Turn is not swapped after game is over`() {
        val givenTurn = BoardPointState.CROSS
        val expectedNextTurn = BoardPointState.CROSS

        gameRules.moveToNextTurn(givenTurn)
        gameRules.reset()

        assertEquals(expectedNextTurn, gameRules.currentUserTurn)
    }

    @Test
    fun `Reset game rules`() {
        val givenTurn = BoardPointState.NOUGHT
        val expectedNextTurn = BoardPointState.NOUGHT

        gameRules.moveToNextTurn(givenTurn)
        gameRules.reset()

        assertNotEquals(expectedNextTurn, gameRules.currentUserTurn)
    }

    @Test
    fun `Draw after placing last point`() {
        val givenBoard = """
            x,x,o
            o,x,x
            _,o,o
        """.trimIndent()
        val givenPoint = BoardPoint(row = 2, column = 0, BoardPointState.CROSS)

        assertTrue(gameRules.isDraw(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User won by 3 in a row`() {
        val givenBoard = """
            x,_,x
            o,_,_
            o,_,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 0, column = 1, BoardPointState.CROSS)

        assertTrue(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User won by 3 in a column`() {
        val givenBoard = """
            o,_,x
            o,x,x
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 2, column = 2, BoardPointState.CROSS)

        assertTrue(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User won by 3 by a diagonal line top-left to bottom-right`() {
        val givenBoard = """
            x,_,o
            o,x,o
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 2, column = 2, BoardPointState.CROSS)

        assertTrue(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User won by 3 by a diagonal line top-right to bottom-left`() {
        val givenBoard = """
            o,_,x
            o,x,o
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 2, column = 0, BoardPointState.CROSS)

        assertTrue(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User did not by 3 in a row`() {
        val givenBoard = """
            x,_,x
            o,x,x
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 0, column = 1, BoardPointState.NOUGHT)

        assertFalse(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User did not win by 3 in a column`() {
        val givenBoard = """
            o,_,x
            o,x,x
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 2, column = 2, BoardPointState.NOUGHT)

        assertFalse(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User did not win by 3 by a diagonal line top-left to bottom-right`() {
        val givenBoard = """
            x,_,o
            o,x,x
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 2, column = 2, BoardPointState.NOUGHT)

        assertFalse(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User did not win by 3 by a diagonal line top-right to bottom-left`() {
        val givenBoard = """
            o,_,x
            x,x,o
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 2, column = 2, BoardPointState.NOUGHT)

        assertFalse(gameRules.isUserWins(givenBoard.asBoard, givenPoint))
    }

    @Test
    fun `User is not allowed to place point on non empty position`() {
        val givenBoard = """
            o,_,x
            x,x,o
            _,o,_
        """.trimIndent()
        val givenPoint = BoardPoint(row = 0, column = 0, BoardPointState.CROSS)

        assertFalse(gameRules.isAllowedToPlacePoint(givenBoard.asBoard, givenPoint))
    }
}