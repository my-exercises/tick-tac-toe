package be.exercise.myexercise.board.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointState
import org.junit.Assert.*
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test

class BoardRepositoryTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    companion object {
        private lateinit var boardRepository: BoardRepository

        @BeforeClass
        @JvmStatic
        fun initialize() {
            boardRepository = BoardRepositoryImpl()
        }
    }

    @Before
    fun beforeEach() {
        boardRepository.cleanBoard()
    }


    @Test
    fun `Constrain board with provided size`() {
        val givenBoardSize = 3
        val expectedSheetSize = givenBoardSize * givenBoardSize

        val constrainedBoard = BoardRepositoryImpl(givenBoardSize)

        assertEquals(expectedSheetSize, constrainedBoard.sheet.flatten().size)
    }

    @Test
    fun `Constrained board contains only empty points`() {
        assertFalse(boardRepository.sheet.flatten().any { !it.isEmpty })
    }

    @Test
    fun `Update point on board`() {
        val givenPoint = BoardPoint(0, 0, BoardPointState.CROSS)

        boardRepository.updatePoint(givenPoint)

        assertTrue(boardRepository.sheet.flatten().any { it == givenPoint })
    }

    @Test
    fun `Invalid point can't be placed on board`() {
        val givenPoint = BoardPoint(5, 0, BoardPointState.CROSS)

        boardRepository.updatePoint(givenPoint)

        assertFalse(boardRepository.sheet.flatten().any { it == givenPoint })
    }

    @Test
    fun `Point is invalid for given board`() {
        val givenPoint = BoardPoint(5, 0, BoardPointState.CROSS)

        assertFalse(boardRepository.isPointValidForBoard(givenPoint))
    }

    @Test
    fun `Verify board is cleaned`() {
        val givenPoint = BoardPoint(0, 0, BoardPointState.CROSS)

        boardRepository.updatePoint(givenPoint)
        boardRepository.cleanBoard()

        assertFalse(boardRepository.sheet.flatten().any { it == givenPoint })
    }
}