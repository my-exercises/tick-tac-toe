package be.exercise.myexercise.board.utils

import org.junit.Assert
import org.junit.Test

class BoardUtilsTest {
    private val boardString = """
        x,_,x
        o,x,o
        o,x,x
        """.trimIndent()

    @Test
    fun `Board conversion vise-versa is correct`() {
        Assert.assertEquals(boardString.asBoard.asString, boardString)
    }
}