package be.exercise.myexercise.board

import be.exercise.myexercise.core.base.Action
import be.exercise.myexercise.core.base.Effect
import be.exercise.myexercise.core.base.State
import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointState

sealed class BoardAction : Action() {
    data class PointClicked(val point: BoardPoint) : BoardAction()
    object ResetBoard : BoardAction()
}

sealed class BoardState : State() {
    data class PointsUpdated(val points: List<BoardPoint>) : BoardState()
    data class TurnChanged(val turn: BoardPointState) : BoardState()
}

sealed class BoardEffect : Effect() {
    data class GameIsOver(val victoryBy: BoardPointState) : BoardEffect()
}