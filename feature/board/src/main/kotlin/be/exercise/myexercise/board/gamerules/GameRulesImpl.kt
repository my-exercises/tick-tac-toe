package be.exercise.myexercise.board.gamerules

import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointState
import be.exercise.myexercise.data.model.BoardPointsListRepresentation

class GameRulesImpl @JvmOverloads constructor(
    private val firstTurn: BoardPointState = BoardPointState.CROSS
) : GameRules {

    private var _currentUserTurn: BoardPointState = firstTurn
    override val currentUserTurn: BoardPointState
        get() = _currentUserTurn

    override fun isUserWins(board: BoardPointsListRepresentation, point: BoardPoint): Boolean {
        // Place new point into board
        val boardWithNewPoint = board.toMutableList().map { it.toMutableList() }
        val (row, column) = point
        boardWithNewPoint[row][column] = point

        return userWinsByRow(boardWithNewPoint, point) ||
                userWinsByColumn(boardWithNewPoint, point) ||
                userWinsByLeftRightDiagonal(boardWithNewPoint, point) ||
                userWinsByRightLeftDiagonal(boardWithNewPoint, point)
    }

    override fun isDraw(board: BoardPointsListRepresentation, point: BoardPoint): Boolean {
        // Place new point into board
        val boardWithNewPoint = board.toMutableList().map { it.toMutableList() }
        val (row, column) = point
        boardWithNewPoint[column][row] = point

        // It's a draw if there are no empty points left and user did not win
        return !boardWithNewPoint.any { it.isEmpty() } && !isUserWins(board, point)
    }

    override fun isAllowedToPlacePoint(
        board: BoardPointsListRepresentation,
        point: BoardPoint
    ): Boolean {
        val (row, column) = point
        // According to rules it's allowed to place point only if position is empty
        return board[row][column].isEmpty
    }

    override fun moveToNextTurn(lastTurn: BoardPointState) {
        _currentUserTurn = determineNextTurn(lastTurn)
    }

    override fun reset() {
        _currentUserTurn = firstTurn
    }

    private fun userWinsByRightLeftDiagonal(
        boardWithNewPoint: BoardPointsListRepresentation,
        point: BoardPoint
    ): Boolean {
        val (row, column, state) = point
        if (column + row != boardWithNewPoint.size - 1) return false
        for (columnIndex in boardWithNewPoint.indices.reversed()) {
            val pointOnBoard =
                boardWithNewPoint[boardWithNewPoint.size - 1 - columnIndex][columnIndex]
            if (pointOnBoard.state != state) break
            // If iteration went till the last element with no breaks, user won
            else if (pointOnBoard.state == state && columnIndex == 0) return true
        }
        return false
    }

    private fun userWinsByLeftRightDiagonal(
        boardWithNewPoint: BoardPointsListRepresentation,
        point: BoardPoint
    ): Boolean {
        val (row, column, state) = point
        if (column != row) return false
        for (diagonalIndex in boardWithNewPoint.indices) {
            val pointOnBoard = boardWithNewPoint[diagonalIndex][diagonalIndex]
            if (pointOnBoard.state != state) break
            // If iteration went till the last element with no breaks, user won
            else if (pointOnBoard.state == state && diagonalIndex == boardWithNewPoint.size - 1) return true
        }
        return false
    }

    private fun userWinsByRow(
        boardWithNewPoint: BoardPointsListRepresentation,
        point: BoardPoint
    ): Boolean {
        val (row, _, state) = point
        for (columnIndex in boardWithNewPoint.indices) {
            val pointOnBoard = boardWithNewPoint[row][columnIndex]
            if (pointOnBoard.state != state) break
            // If iteration went till the last element with no breaks, user won
            else if (pointOnBoard.state == state && columnIndex == boardWithNewPoint.size - 1) return true
        }
        return false
    }

    private fun userWinsByColumn(
        boardWithNewPoint: BoardPointsListRepresentation,
        point: BoardPoint
    ): Boolean {
        val (_, column, state) = point
        for (rowIndex in boardWithNewPoint.indices) {
            val pointOnBoard = boardWithNewPoint[rowIndex][column]
            if (pointOnBoard.state != state) break
            // If iteration went till the last element with no breaks, user won
            else if (pointOnBoard.state == state && rowIndex == boardWithNewPoint.size - 1) return true
        }
        return false
    }

    private fun determineNextTurn(turn: BoardPointState): BoardPointState {
        return when (turn) {
            BoardPointState.CROSS -> BoardPointState.NOUGHT
            BoardPointState.NOUGHT -> BoardPointState.CROSS
            else -> BoardPointState.CROSS
        }
    }
}