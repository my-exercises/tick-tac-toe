package be.exercise.myexercise.board.gamerules

import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointState
import be.exercise.myexercise.data.model.BoardPointsListRepresentation


/**
 * Pure logic layer which controls game rules such as
 * 1. Who should make next turn?
 * 2. Is there a winner?
 * Should NOT handle board logic
 */
interface GameRules {
    val currentUserTurn: BoardPointState
    fun isUserWins(board: BoardPointsListRepresentation, point: BoardPoint): Boolean
    fun moveToNextTurn(lastTurn: BoardPointState)
    fun isDraw(board: BoardPointsListRepresentation, point: BoardPoint): Boolean
    fun isAllowedToPlacePoint(board: BoardPointsListRepresentation, point: BoardPoint): Boolean

    // Reset game rules to defaults
    fun reset()
}