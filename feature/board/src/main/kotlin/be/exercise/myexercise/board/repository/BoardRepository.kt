package be.exercise.myexercise.board.repository

import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointsListRepresentation


interface BoardRepository {
    val sheet: BoardPointsListRepresentation

    fun updatePoint(point: BoardPoint)
    fun isPointValidForBoard(point: BoardPoint): Boolean
    fun cleanBoard()
}