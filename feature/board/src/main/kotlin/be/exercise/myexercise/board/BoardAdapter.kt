package be.exercise.myexercise.board

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.ui_components.PointView

class BoardAdapter(private val onPointClick: (BoardPoint) -> Unit) : BaseAdapter() {
    private var items: List<BoardPoint> = listOf()

    fun pushPoints(points: List<BoardPoint>) {
        items = points
        notifyDataSetChanged()
    }

    override fun getCount(): Int = items.size

    override fun getItem(position: Int): BoardPoint = items[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        if (parent?.context == null) return null
        return with(getItem(position)) {
            PointView(parent.context).also {
                it.bindPoint(this, onClick = onPointClick)
            }
        }
    }
}