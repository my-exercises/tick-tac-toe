package be.exercise.myexercise.board

import android.content.res.Resources
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import be.exercise.myexercise.board.databinding.FragmentBoardBinding
import be.exercise.myexercise.board.repository.BoardRepositoryImpl.Companion.DEFAULT_BOARD_SIZE
import be.exercise.myexercise.core.base.Effect
import be.exercise.myexercise.core.base.State
import be.exercise.myexercise.core.base.viewBinding
import be.exercise.myexercise.core.extensions.observe
import be.exercise.myexercise.data.model.BoardPointState
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentBoard : Fragment(R.layout.fragment_board) {
    private val fragmentViewModel: BoardViewModel by viewModel()
    private val boardAdapter by lazy {
        BoardAdapter {
            fragmentViewModel.processAction(BoardAction.PointClicked(it))
        }
    }
    private val binding: FragmentBoardBinding by viewBinding(FragmentBoardBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBoard()
        setupListeners()
        setupObservers()
        cleanBoard()
    }

    private fun setupBoard() {
        binding.gridBoard.apply {
            numColumns = DEFAULT_BOARD_SIZE
            adapter = boardAdapter
        }
    }

    private fun setupListeners() {
        binding.buttonResetBoard.setOnClickListener {
            cleanBoard()
        }
    }

    private fun setupObservers() {
        viewLifecycleOwner.observe(fragmentViewModel.state, ::updateState)
        viewLifecycleOwner.observe(fragmentViewModel.effect, ::updateEffect)
    }

    private fun updateState(state: State) {
        when (state) {
            is BoardState.TurnChanged -> {
                binding.textBoardHeader.text = state.turn.toString()
            }
            is BoardState.PointsUpdated -> {
                boardAdapter.pushPoints(state.points)
            }
        }
    }

    private fun updateEffect(effect: Effect) {
        when (effect) {
            is BoardEffect.GameIsOver -> {
                showGameOver(effect.victoryBy)
            }
        }
    }

    private fun cleanBoard() {
        fragmentViewModel.processAction(BoardAction.ResetBoard)
    }

    private fun showGameOver(victoryBy: BoardPointState) {
        val title = when (victoryBy) {
            BoardPointState.EMPTY -> getString(R.string.no_victory)
            else -> "$victoryBy ${getString(R.string.won)}"
        }
        MaterialAlertDialogBuilder(requireContext(), R.style.Theme_MaterialComponents_Dialog_Alert)
            .setView(R.layout.view_game_over)
            .setTitle(title)
            .setCancelable(true)
            .setNeutralButton(getString(R.string.close)) { dialog, _ -> dialog.dismiss() }
            .setPositiveButton(getString(R.string.play_again)) { dialog, _ ->
                cleanBoard()
                dialog.dismiss()
            }
            .create()
            .show()
    }
}