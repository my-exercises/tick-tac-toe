package be.exercise.myexercise.board.di

import be.exercise.myexercise.board.BoardViewModel
import be.exercise.myexercise.board.gamerules.GameRules
import be.exercise.myexercise.board.gamerules.GameRulesImpl
import be.exercise.myexercise.board.repository.BoardRepository
import be.exercise.myexercise.board.repository.BoardRepositoryImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val boardModule = module {
    single<GameRules> {
        GameRulesImpl()
    }

    single<BoardRepository> {
        BoardRepositoryImpl()
    }

    viewModel {
        BoardViewModel(get(), get(), get())
    }
}