package be.exercise.myexercise.board

import androidx.lifecycle.viewModelScope
import be.exercise.myexercise.board.gamerules.GameRules
import be.exercise.myexercise.board.repository.BoardRepository
import be.exercise.myexercise.core.base.BaseViewModel
import be.exercise.myexercise.data.model.BoardPoint
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class BoardViewModel(
    private val ioDispatcher: CoroutineDispatcher,
    private val boardRepository: BoardRepository,
    private val gameRules: GameRules,
) : BaseViewModel<BoardAction>() {

    override fun processAction(action: BoardAction) {
        when (action) {
            is BoardAction.ResetBoard -> handleResetBoard()
            is BoardAction.PointClicked -> handlePointUpdate(action.point)
        }
    }

    private fun handlePointUpdate(point: BoardPoint) {
        viewModelScope.launch(ioDispatcher) {
            if (!gameRules.isAllowedToPlacePoint(boardRepository.sheet, point)) return@launch

            val pointWithTurn = point.withChangedState(gameRules.currentUserTurn)
            boardRepository.updatePoint(pointWithTurn).also {
                if (gameRules.isUserWins(boardRepository.sheet, pointWithTurn)) {
                    dispatchEffect(BoardEffect.GameIsOver(pointWithTurn.state))
                }
                swapUserTurn()
                dispatchState(BoardState.PointsUpdated(boardRepository.sheet.flatten()))
                dispatchState(BoardState.TurnChanged(gameRules.currentUserTurn))
            }
        }
    }

    private fun handleResetBoard() {
        viewModelScope.launch(ioDispatcher) {
            gameRules.reset()
            boardRepository.cleanBoard().also {
                dispatchState(BoardState.TurnChanged(gameRules.currentUserTurn))
                dispatchState(BoardState.PointsUpdated(boardRepository.sheet.flatten()))
            }
        }
    }

    private fun swapUserTurn() = gameRules.moveToNextTurn(gameRules.currentUserTurn)

}