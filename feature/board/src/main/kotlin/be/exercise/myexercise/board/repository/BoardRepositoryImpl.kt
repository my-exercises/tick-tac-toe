package be.exercise.myexercise.board.repository

import be.exercise.myexercise.data.model.BoardPoint
import be.exercise.myexercise.data.model.BoardPointsListRepresentation

class BoardRepositoryImpl @JvmOverloads constructor(
    private val boardSize: Int = DEFAULT_BOARD_SIZE
) : BoardRepository {

    companion object {
        internal const val DEFAULT_BOARD_SIZE = 3

        private fun constrainCleanBoard(size: Int) = MutableList(size) { row ->
            MutableList(size) { column ->
                BoardPoint.createEmpty(
                    x = row,
                    y = column
                )
            }
        }
    }

    private val _sheet = constrainCleanBoard(boardSize)

    override val sheet: BoardPointsListRepresentation
        get() = _sheet

    override fun updatePoint(point: BoardPoint) {
        if (!isPointValidForBoard(point)) return
        val (row, column) = point
        _sheet[row][column] = point
    }

    override fun isPointValidForBoard(point: BoardPoint): Boolean {
        return point.row < boardSize && point.column < boardSize && sheet[point.row][point.column].isEmpty
    }

    override fun cleanBoard() {
        with(_sheet) {
            clear()
            addAll(constrainCleanBoard(boardSize))
        }
    }
}