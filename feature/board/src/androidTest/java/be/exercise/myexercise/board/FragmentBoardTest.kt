package be.exercise.myexercise.board

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.DataInteraction
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import be.exercise.myexercise.board.di.boardModule
import be.exercise.myexercise.data.model.BoardPointState
import com.agoda.kakao.common.actions.BaseActions
import com.agoda.kakao.common.utilities.getResourceString
import com.agoda.kakao.dialog.KAlertDialog
import com.agoda.kakao.list.KAbsListView
import com.agoda.kakao.list.KAdapterItem
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.screen.Screen.Companion.onScreen
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import io.mockk.mockkClass
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.mock.MockProviderRule

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class FragmentBoardTest : KoinTest {

    private val dispatcher = TestCoroutineDispatcher()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(
            module {
                // Use test coroutine dispatcher
                single<CoroutineDispatcher> { dispatcher }
            } + boardModule
        )
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        mockkClass(clazz)
    }

    @Before
    fun before() {
        Dispatchers.setMain(dispatcher)
    }

    @Test
    fun verifyBoardResetByButton() {
        launchFragmentInContainer<FragmentBoard>()
        onScreen<BoardScreenUI> {
            board.childAt<BoardScreenUI.PointItem>(0) { click() }
            buttonResetBoard.click()

            board {
                children<BoardScreenUI.PointItem> {
                    pointText { hasText(BoardPointState.EMPTY.toString()) }
                }
            }
        }
    }

    @Test
    fun verifyTurnResetByButton() {
        launchFragmentInContainer<FragmentBoard>()
        onScreen<BoardScreenUI> {
            board.childAt<BoardScreenUI.PointItem>(0) { click() }
            buttonResetBoard.click()

            userTurn { hasText(BoardPointState.CROSS.toString()) }
        }
    }

    @Test
    fun verifyPointIsPlacedOnTheBoard() {
        launchFragmentInContainer<FragmentBoard>()
        onScreen<BoardScreenUI> {
            board.childAt<BoardScreenUI.PointItem>(0) {
                click()
                pointText { hasText(BoardPointState.CROSS.toString()) }
            }
        }
    }

    @Test
    fun verifyUserTurnIsChangedAfterPointPlacement() {
        launchFragmentInContainer<FragmentBoard>()
        onScreen<BoardScreenUI> {
            board.childAt<BoardScreenUI.PointItem>(0) { click() }
            userTurn { hasText(BoardPointState.NOUGHT.toString()) }
        }
    }

    @Test
    fun verifyDialogIsDisplayedByWin() {
        launchFragmentInContainer<FragmentBoard>()
        onScreen<BoardScreenUI> {
            var leftPoints = 5 // Click 5 times on points one by one
            board.children<BoardScreenUI.PointItem> {
                if (leftPoints >= 0) {
                    click()
                    leftPoints -= 1
                }
            }
            // Click on last point to get win by diagonal
            board.childAt<BoardScreenUI.PointItem>(6) { click() }
            alertDialog.isDisplayed()
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }


    class BoardScreenUI : Screen<BoardScreenUI>() {
        val buttonResetBoard = KButton { withId(R.id.buttonResetBoard) }
        val userTurn = KTextView { withId(R.id.textBoardHeader) }
        val board: KAbsListView = KAbsListView({
            withId(R.id.gridBoard)
        }, itemTypeBuilder = {
            itemType(::PointItem)
        })
        val alertDialog = KAlertDialog()

        class PointItem(parent: DataInteraction) : KAdapterItem<PointItem>(parent) {
            val pointText: KTextView = KTextView(parent) { withId(R.id.textPointView) }
        }
    }

}