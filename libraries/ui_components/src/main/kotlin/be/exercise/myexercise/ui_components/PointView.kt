package be.exercise.myexercise.ui_components

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import be.exercise.myexercise.data.model.BoardPoint
import kotlinx.android.synthetic.main.point_view.view.*


class PointView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.point_view, this)
    }

    fun bindPoint(point: BoardPoint, onClick: (point: BoardPoint) -> Unit) {
        textPointView.text = "${point.state}"
        setOnClickListener { onClick(point) }
    }

    /**
     * Point View will be used in a grid layout, where width is calculated, but height not
     * To keep view square, measure only by width
     */
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        measuredWidth.let { width -> setMeasuredDimension(width, width) }
    }
}