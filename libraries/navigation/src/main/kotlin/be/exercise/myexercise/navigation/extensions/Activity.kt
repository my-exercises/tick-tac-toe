package be.exercise.myexercise.navigation.extensions

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment

fun AppCompatActivity.getNavigationHost(@IdRes idRes: Int): NavHostFragment? {
    return supportFragmentManager.findFragmentById(idRes) as? NavHostFragment
}