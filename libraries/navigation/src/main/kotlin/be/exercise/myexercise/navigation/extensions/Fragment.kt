package be.exercise.myexercise.navigation

import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import timber.log.Timber

val defaultNavOptions = NavOptions
    .Builder()
    .setEnterAnim(R.anim.nav_default_enter_anim)
    .setExitAnim(R.anim.nav_default_exit_anim)
    .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
    .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
    .build()

fun Fragment.navigateTo(action: NavigationAction, popBackStack: Boolean = false) = findNavController().apply {
    if (popBackStack) {
        popBackStack()
    }
}.navigate(Uri.parse(action.uri()), defaultNavOptions)

fun Fragment.navigateBack() {
    findNavController().navigateUp()
}

fun Fragment.navigateTo(action: NavDirections) {
    val navController = findNavController()
    try {
        navController.navigate(action)
    } catch (exception: Exception) {
        val actionNotFound = navController.previousBackStackEntry?.destination?.getAction(action.actionId) == null
        when {
            actionNotFound && BuildConfig.DEBUG -> throw exception
            actionNotFound -> Timber.w(exception, "Navigation action/destination not found")
            else -> Timber.w(exception, "Navigation action/destination duplicated")
        }
    }
}