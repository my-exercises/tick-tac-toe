package be.exercise.myexercise.navigation

interface NavigationUri {
    fun uri(): String
}

sealed class NavigationAction : NavigationUri {
    object Board : NavigationAction() {
        override fun uri() = "myexercise://board/"
    }

    class Uri(private val rawUri: String) : NavigationAction() {
        override fun uri() = rawUri
    }
}