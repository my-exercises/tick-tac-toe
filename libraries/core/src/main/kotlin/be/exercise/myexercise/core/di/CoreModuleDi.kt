package be.exercise.myexercise.core.di

import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

internal val coreModuleDi = module {
    single {
        Dispatchers.IO
    }
}