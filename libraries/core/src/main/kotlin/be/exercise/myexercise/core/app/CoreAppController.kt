package be.exercise.myexercise.core.app

import android.app.Application
import be.exercise.myexercise.core.di.coreModuleDi
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

open class CoreAppController : Application() {

    protected open val diModules: List<Module> = listOf()

    override fun onCreate() {
        super.onCreate()

        // Initialize dependency inject
        startKoin {
            androidContext(this@CoreAppController)
            modules(
                listOf(
                    coreModuleDi
                ).plus(diModules)
            )
        }
    }
}