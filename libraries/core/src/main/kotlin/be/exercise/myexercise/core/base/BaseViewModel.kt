package be.exercise.myexercise.core.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Align interactions with ViewModels across application
 * and secure operations on dispatchers
 */
abstract class BaseViewModel<T : Action> : ViewModel() {

    private val _state by lazy { MutableLiveData<State>() }
    val state: LiveData<State> get() = _state

    private val _effect by lazy { SingleLiveEvent<Effect>() }
    val effect: LiveData<Effect> get() = _effect

    fun dispatchState(state: State) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                _state.value = state
            }
        }
    }

    fun dispatchEffect(effect: Effect) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                _effect.value = effect
            }
        }
    }

    abstract fun processAction(action: T)
}