package be.exercise.myexercise.core.di

import kotlinx.coroutines.CoroutineDispatcher
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.koin.core.logger.Level
import org.koin.test.*
import org.koin.test.category.CheckModuleTest
import org.koin.test.check.checkModules

@Category(CheckModuleTest::class)
class CoreModuleDiTest : AutoCloseKoinTest() {
    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger(Level.ERROR)
        modules(coreModuleDi)
    }

    @Test
    fun `Verify core module`() {
        checkModules {
            modules(coreModuleDi)
        }
    }

    @Test
    fun `Verify CoroutineDispatcher is declared`() {
        assertNotNull(get<CoroutineDispatcher>())
    }
}