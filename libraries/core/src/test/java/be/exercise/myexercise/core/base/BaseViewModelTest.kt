package be.exercise.myexercise.core.base

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.reset
import org.mockito.Mockito.verify
import org.mockito.Mockito.times

sealed class StateForTest : State() {
    object InitialState : StateForTest()
}

sealed class EffectForTest: Effect() {
    object InvokedEffect: EffectForTest()
}

@ExperimentalCoroutinesApi
class BaseViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val dispatcher = TestCoroutineDispatcher()

    companion object {
        private lateinit var stateObserver: Observer<State>
        private lateinit var effectObserver: Observer<Effect>
        private lateinit var viewModel: BaseViewModel<Action>

        @BeforeClass
        @JvmStatic
        fun initialize() {
            viewModel = object : BaseViewModel<Action>() {
                override fun processAction(action: Action) {}
            }
            stateObserver = mock()
            effectObserver = mock()
        }

        @JvmStatic
        fun resetMocks() {
            reset(stateObserver)

            viewModel.state.observeForever(stateObserver)
            viewModel.effect.observeForever(effectObserver)
        }
    }

    @Before
    fun before() {
        Dispatchers.setMain(dispatcher)
        resetMocks()
    }

    @Test
    fun `Verify state dispatch`() {
        val expectedState = StateForTest.InitialState

        viewModel.dispatchState(expectedState)

        val captor = ArgumentCaptor.forClass(expectedState::class.java)
        captor.run {
            verify(stateObserver, times(1)).onChanged(capture())
            assertEquals(expectedState, value)
        }
    }

    @Test
    fun `Verify effect dispatch`() {
        val expectedEffect = EffectForTest.InvokedEffect

        viewModel.dispatchEffect(expectedEffect)

        val captor = ArgumentCaptor.forClass(expectedEffect::class.java)
        captor.run {
            verify(effectObserver, times(1)).onChanged(capture())
            assertEquals(expectedEffect, value)
        }
    }


    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}