package be.exercise.myexercise.core.base

import org.mockito.Mockito

inline fun <reified T> mock(): T = Mockito.mock(T::class.java)
