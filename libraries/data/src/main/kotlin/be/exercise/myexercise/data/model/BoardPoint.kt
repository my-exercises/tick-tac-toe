package be.exercise.myexercise.data.model

typealias BoardPointsListRepresentation = List<List<BoardPoint>>

enum class BoardPointState {
    CROSS {
        override fun toString(): String = "x"
    },
    NOUGHT {
        override fun toString(): String = "o"
    },
    EMPTY {
        override fun toString(): String = "_"
    }
}

data class BoardPoint(
    val row: Int,
    val column: Int,
    val state: BoardPointState,
) {
    val isEmpty = state == BoardPointState.EMPTY

    fun withChangedState(newState: BoardPointState): BoardPoint {
        return copy(state = newState)
    }

    companion object {
        @JvmStatic
        fun createEmpty(x: Int, y: Int) = BoardPoint(x, y, BoardPointState.EMPTY)
    }
}