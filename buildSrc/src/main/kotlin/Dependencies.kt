object Config {
    const val applicationID = "be.exercise.myexercise"
    const val buildToolsVersion = "30.0.3"
    const val compileSdkVersion = 30
    const val minSdkVersion = 22
    const val targetSdkVersion = 30
    const val versionCode = 1
    const val versionName = "0.1"
    const val jvmTarget = "1.8"
}

object Versions {
    const val gradle_plugin = "7.0.2"
    const val kotlin = "1.5.31"
    const val kotlin_coroutines = "1.5.2"
    const val core_ktx = "1.6.0"
    const val timber = "5.0.1"
    const val koin = "2.2.2"

    const val material = "1.4.0"
    const val androidx_lifecycle = "2.3.0"
    const val androidx_fragment = "1.3.6"
    const val androidx_navigation = "2.3.5"
    const val androidx_constraint_layout = "2.0.4"

    const val junit = "4.13.2"
    const val junit_ktx = "1.1.3"
    const val androidx_core_testing = "2.1.0"
    const val mockito = "3.9.0"
    const val coroutines_testing = "1.4.2"
    const val espresso = "3.3.0"
    const val kakao = "2.3.4"
    const val mockk = "1.12.0"
}

object Modules {
    private const val libraryPkg = "libraries"
    private const val featurePkg = "feature"

    const val app = ":app"

    // Libraries
    const val data = ":${libraryPkg}:data"
    const val core = ":${libraryPkg}:core"
    const val ui_components = ":${libraryPkg}:ui_components"
    const val navigation = ":${libraryPkg}:navigation"

    // Features
    const val board = ":${featurePkg}:board"
}

object Deps {
    // Kotlin
    const val kotlin_core = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val kotlin_coroutines =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.kotlin_coroutines}"
    const val kotlin_extensions_runtime =
        "org.jetbrains.kotlin:kotlin-android-extensions-runtime:${Versions.kotlin}"

    // AndroidX
    const val androidx_core_ktx = "androidx.core:core-ktx:${Versions.core_ktx}"
    const val androidx_fragment = "androidx.fragment:fragment-ktx:${Versions.androidx_fragment}"
    const val androidx_navigation_ui =
        "androidx.navigation:navigation-ui-ktx:${Versions.androidx_navigation}"
    const val androidx_navigation_fragment =
        "androidx.navigation:navigation-fragment-ktx:${Versions.androidx_navigation}"
    const val androidx_lifecycle_runtime =
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.androidx_lifecycle}"
    const val androidx_lifecycle_common =
        "androidx.lifecycle:lifecycle-common-java8:${Versions.androidx_lifecycle}"
    const val androidx_lifecycle_livedata =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.androidx_lifecycle}"
    const val androidx_lifecycle_view_model =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.androidx_lifecycle}"
    const val androidx_lifecycle_extension =
        "androidx.lifecycle:lifecycle-extensions:${Versions.androidx_lifecycle}"
    const val androidx_constraint_layout =
        "androidx.constraintlayout:constraintlayout:${Versions.androidx_constraint_layout}"

    // Google
    const val material = "com.google.android.material:material:${Versions.material}"

    // DI
    const val koin = "io.insert-koin:koin-android:${Versions.koin}"
    const val koin_viewmodel = "io.insert-koin:koin-androidx-viewmodel:${Versions.koin}"

    // Utils
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    // Testing
    const val testing_junit = "junit:junit:${Versions.junit}"
    const val testing_junit_ktx = "androidx.test.ext:junit-ktx:${Versions.junit_ktx}"
    const val testing_espresso_core = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val testing_androidx_core =
        "androidx.arch.core:core-testing:${Versions.androidx_core_testing}"
    const val testing_mockito = "org.mockito:mockito-core:${Versions.mockito}"
    const val testing_coroutines =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines_testing}"
    const val testing_fragment = "androidx.fragment:fragment-testing:${Versions.androidx_fragment}"
    const val testing_koin = "io.insert-koin:koin-test:${Versions.koin}"
    const val testing_kakao = "com.agoda.kakao:kakao:${Versions.kakao}"
    const val testing_mockk_android = "io.mockk:mockk-android:${Versions.mockk}"
    const val testing_mockk_android_jvm = "io.mockk:mockk-agent-jvm:${Versions.mockk}"
}